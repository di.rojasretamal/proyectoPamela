/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Duoc
 */
public class TicketVO {

    private int idTicket;
    private String descripcionTicket;

    public TicketVO() {
    }

    public TicketVO(int idTicket, String descripcionTicket) {
        this.idTicket = idTicket;
        this.descripcionTicket = descripcionTicket;
    }
    
    
    /**
     * @return the idTicket
     */
    public int getIdTicket() {
        return idTicket;
    }

    /**
     * @param idTicket the idTicket to set
     */
    public void setIdTicket(int idTicket) {
        this.idTicket = idTicket;
    }

    /**
     * @return the descripcionTicket
     */
    public String getDescripcionTicket() {
        return descripcionTicket;
    }

    /**
     * @param descripcionTicket the descripcionTicket to set
     */
    public void setDescripcionTicket(String descripcionTicket) {
        this.descripcionTicket = descripcionTicket;
    }
    
   
    
}
