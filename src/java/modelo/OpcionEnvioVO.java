/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Duoc
 */
public class OpcionEnvioVO {

    private int idEnvio;
    private String tipoEnvio;

    public OpcionEnvioVO() {
    }

    public OpcionEnvioVO(int idEnvio, String tipoEnvio) {
        this.idEnvio = idEnvio;
        this.tipoEnvio = tipoEnvio;
    }
    
    
    /**
     * @return the idEnvio
     */
    public int getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(int idEnvio) {
        this.idEnvio = idEnvio;
    }

    /**
     * @return the tipoEnvio
     */
    public String getTipoEnvio() {
        return tipoEnvio;
    }

    /**
     * @param tipoEnvio the tipoEnvio to set
     */
    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }
    
    
    
    
}