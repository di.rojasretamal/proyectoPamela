package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;

/**
 *
 * @author Nicolas
 */
public class EstacionamientoADO {
    
    private Connection cx;
    public EstacionamientoADO(Connection cx)
    {
        this.cx =cx;
    }
    
    public int ingresarEstacionamiento(EstacionamientoVO estacionamiento)
    {
        String sql = "insert into estacionamiento values(?,?,?,?)";
  
        try {
          
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, estacionamiento.getIdEstacionamiento());
            ps.setString(2, estacionamiento.getTipoEstacionamiento());
            ps.setInt(3, estacionamiento.getNumeroTicket());
            ps.setInt(4, estacionamiento.getValorHora());
            
            
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(ClienteADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public int eliminarEstacionamiento(int idEstacionamiento)
    {
        String sql = "delete from estacionamiento where idEstacionamiento=?";
        
        try {
            
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, idEstacionamiento);
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(EstacionamientoADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public int actualizarEstacionamiento(EstacionamientoVO estacionamiento)
    {
       
        String sql="update estacionamiento set tipoEstacionamiento=? where idEstacionamiento=?";
        try {
            PreparedStatement ps= this.cx.prepareStatement(sql);
            ps.setInt(1, estacionamiento.getIdEstacionamiento());
            ps.setString(2, estacionamiento.getTipoEstacionamiento());
            ps.setInt(3, estacionamiento.getNumeroTicket());
            ps.setInt(4, estacionamiento.getValorHora());
            
            
            int c=ps.executeUpdate();
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(EstacionamientoADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public ArrayList<EstacionamientoVO> buscarTodos(){
            ArrayList<EstacionamientoVO> temp=new ArrayList<EstacionamientoVO>();
                    String sql="select * from estacionamiento";
                    Statement stm = null;
                    ResultSet rs =null;
        try {
                    stm= this.cx.createStatement();
                    rs=stm.executeQuery(sql);
                    while(rs.next()){
                        temp.add(new EstacionamientoVO(rs.getInt(1),rs.getString(2),rs.getInt(3), rs.getInt(4)));
                    }
                    rs.close();
                    stm.close();
                    return temp;
                    
        } catch (SQLException ex) {
            Logger.getLogger(EstacionamientoADO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }


    }
    
}

