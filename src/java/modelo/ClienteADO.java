package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas
 */
public class ClienteADO {
    
    private Connection cx;
    public ClienteADO(Connection cx)
    {
        this.cx =cx;
    }
    
    public int ingresarCliente(ClienteVO cliente)
    {
        String sql = "insert into cliente values(?,?,?,?,?,?,?)";
        
        try {
          
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, cliente.getIdCliente());
            ps.setString(2, cliente.getRutCliente());
            ps.setString(3, cliente.getNombreCliente() + " " + cliente.getApellidoCliente());
            ps.setString(4, cliente.getApellidoCliente());
            ps.setString(5, cliente.getDireccionCliente());
            ps.setString(6, cliente.getTelefonoCliente());
            ps.setString(7, cliente.getCorreoElectronico());
            
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(ClienteADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public int eliminarCliente(int idCliente)
    {
        String sql = "delete from cliente where idCliente=?";
        
        try {
            
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, idCliente);
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(ClienteADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public int actualizarCliente(ClienteVO cliente)
    {
       
        String sql="update cliente set nombreCliente=? where idCliente=?";
        try {
            PreparedStatement ps= this.cx.prepareStatement(sql);
            ps.setInt(1, cliente.getIdCliente());
            ps.setString(2, cliente.getRutCliente());
            ps.setString(3, cliente.getNombreCliente());
            ps.setString(4, cliente.getApellidoCliente());
            ps.setString(5, cliente.getDireccionCliente());
            ps.setString(6, cliente.getTelefonoCliente());
            ps.setString(7, cliente.getCorreoElectronico());
            
            int c=ps.executeUpdate();
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(ClienteADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public ArrayList<ClienteVO> buscarTodos(){
            ArrayList<ClienteVO> temp=new ArrayList<ClienteVO>();
                    String sql="select * from cliente";
                    Statement stm = null;
                    ResultSet rs =null;
        try {
                    stm= this.cx.createStatement();
                    rs=stm.executeQuery(sql);
                    while(rs.next()){
                        temp.add(new ClienteVO(rs.getInt(1),rs.getString(2),rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
                    }
                    rs.close();
                    stm.close();
                    return temp;
                    
        } catch (SQLException ex) {
            Logger.getLogger(ClienteADO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
}
}
