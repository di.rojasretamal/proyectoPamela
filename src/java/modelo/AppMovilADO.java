/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas
 */
public class AppMovilADO {

    private Connection cx;

    private final static Logger logger = Logger.getLogger(AppMovilADO.class.getName());

    public AppMovilADO(Connection cx) {
        this.cx = cx;
    }

    public int insertarDatos(AppMovilVO datos) {
        String sql = "INSERT INTO appMovil VALUES(?,?)";

        try {

            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, datos.getIdCliente());
            ps.setInt(2, datos.getValorTotal());

            int c = ps.executeUpdate();

            return c;
        } catch (SQLException ex) {
            logger.info("Esta la caga." + ex);
            throw new RuntimeException("Esta la Caga." + ex);
        } finally {
            sql = null;
        }
    }

    public int eliminarDatos(int idCliente) {
        String sql = "delete from appMovil where idCliente=?";

        try {

            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, idCliente);
            int c = ps.executeUpdate();

            return c;
        } catch (SQLException ex) {
            logger.info("Esta la caga." + ex);
            throw new RuntimeException("Esta la Caga." + ex);
        }
    }

    public int actualizarDatos(AppMovilVO datos) {

        String sql = "update appMovil set valorTotal=? where idCliente=?";
        try {
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, datos.getIdCliente());
            ps.setInt(2, datos.getValorTotal());

            int c = ps.executeUpdate();
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(AppMovilADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public ArrayList<AppMovilVO> buscarTodos() {
        ArrayList<AppMovilVO> temp = new ArrayList<AppMovilVO>();
        String sql = "select * from appMovil"; //Esto es feo
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = this.cx.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                temp.add(new AppMovilVO(rs.getInt(1), rs.getInt(2)));
            }
            rs.close();
            stm.close();
            return temp;

        } catch (SQLException ex) {
            Logger.getLogger(AppMovilADO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
