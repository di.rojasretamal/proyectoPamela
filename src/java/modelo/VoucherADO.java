package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas
 */
public class VoucherADO {

    private Connection cx;

    private final static Logger logger = Logger.getLogger(VoucherADO.class.getName());

    public VoucherADO(Connection cx) {
        this.cx = cx;
    }

    public int ingresarVoucher(VoucherVO voucher) {
        String sql = "insert into voucher values(?,?,?,?)";
        /*private int idCliente;
    private int idVoucher;
    private EstacionamientoVO estacionamiento;
    private int valorTotal;*/
        try {

            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, voucher.getIdCliente());
            ps.setInt(2, voucher.getIdVoucher());
            ps.setString(3, voucher.getEstacionamiento());
            ps.setInt(4, voucher.getValorTotal());

            int c = ps.executeUpdate();

            return c;
        } catch (Exception ex) {
            logger.info("Quedo la caga. " + ex);
            throw new RuntimeException("Quedo la caga. " + ex);
        }
    }

    public int eliminarVoucher(int idVoucher) {
        String sql = "DELETE FROM voucher WHEER idVoucher = ? ";

        try {

            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, idVoucher);
            int c = ps.executeUpdate();

            return c;
        } catch (SQLException ex) {

            Logger.getLogger(VoucherADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public int actualizarVoucher(VoucherVO voucher) {

        String sql = "update voucher set idVoucher=? where idVoucher=?";
        try {
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, voucher.getIdCliente());
            ps.setInt(2, voucher.getIdVoucher());
            ps.setString(3, voucher.getEstacionamiento());
            ps.setInt(4, voucher.getValorTotal());

            int c = ps.executeUpdate();
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(VoucherADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public ArrayList<VoucherVO> buscarTodos() {
        ArrayList<VoucherVO> temp = new ArrayList<VoucherVO>();
        String sql = "select * from voucher";
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = this.cx.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                temp.add(new VoucherVO(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4)));
            }
            rs.close();
            stm.close();
            return temp;

        } catch (SQLException ex) {
            Logger.getLogger(VoucherADO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
