/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Duoc
 */
public class TicketADO {
    
    private Connection cx;
    public TicketADO(Connection cx)
    {
        this.cx =cx;
    }

    public int generarTicketPago(TicketVO ticket)
    {
        String sql = "insert into ticket values(?,?)";
        
        try {
          
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, ticket.getIdTicket());
            ps.setString(2, ticket.getDescripcionTicket());
            
            
            
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(TicketADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public int eliminarTicket(int idTicket)
    {
        String sql = "delete from ticket where idTicket=?";
        
        try {
            
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, idTicket);
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(TicketADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public int actualizarTicket(TicketVO ticket, ClienteVO cliente)
    {
       
        String sql= "update ticket set descripcionTicket=? where idTicket=?";
        try {
            PreparedStatement ps= this.cx.prepareStatement(sql);
            ps.setInt(1, ticket.getIdTicket());
            ps.setString(2, ticket.getDescripcionTicket());
            
            
            int c=ps.executeUpdate();
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(TicketADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public ArrayList<TicketVO> buscarTodos(){
            ArrayList<TicketVO> temp=new ArrayList<TicketVO>();
                    String sql="select * from ticket";
                    Statement stm = null;
                    ResultSet rs =null;
        try {
                    stm= this.cx.createStatement();
                    rs=stm.executeQuery(sql);
                    while(rs.next()){
                        temp.add(new TicketVO(rs.getInt(1),rs.getString(2)));
                    }
                    rs.close();
                    stm.close();
                    return temp;
                    
        } catch (SQLException ex) {
            Logger.getLogger(TicketADO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
}
}
