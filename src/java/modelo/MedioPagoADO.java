/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Duoc
 */
public class MedioPagoADO {
    
    private Connection cx;
    public MedioPagoADO(Connection cx)
    {
        this.cx =cx;
    }
    /*
    private int idPago;
    private int idCliente;
    private String tipoPago;*/
    public int ingresarMedioPago(MedioPagoVO medioPago, ClienteVO cliente)
    {
        String sql = "insert into medioPago values(?,?,?)";
        
        try {
          
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, medioPago.getIdPago());
            ps.setInt(2, cliente.getIdCliente());
            ps.setString(3, medioPago.getTipoPago());
            
            
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(MedioPagoADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public int eliminarMedioPago(int idPago)
    {
        String sql = "delete from medioPago where idPago=?";
        
        try {
            
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, idPago);
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(ClienteADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public int actualizarMedioPago(MedioPagoVO medioPago, ClienteVO cliente)
    {
       
        String sql= "update medioPago set tipoPago=? where idPago=?";
        try {
            PreparedStatement ps= this.cx.prepareStatement(sql);
            ps.setInt(1, medioPago.getIdPago());
            ps.setInt(2, cliente.getIdCliente());
            ps.setString(3, medioPago.getTipoPago());
            
            int c=ps.executeUpdate();
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(MedioPagoADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public ArrayList<MedioPagoVO> buscarTodos(){
            ArrayList<MedioPagoVO> temp=new ArrayList<MedioPagoVO>();
                    String sql="select * from medioPago";
                    Statement stm = null;
                    ResultSet rs =null;
        try {
                    stm= this.cx.createStatement();
                    rs=stm.executeQuery(sql);
                    while(rs.next()){
                        temp.add(new MedioPagoVO(rs.getInt(1),rs.getInt(2),rs.getString(3)));
                    }
                    rs.close();
                    stm.close();
                    return temp;
                    
        } catch (SQLException ex) {
            Logger.getLogger(MedioPagoADO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
}
}
