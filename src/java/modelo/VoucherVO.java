/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nicolas
 */
public class VoucherVO {

    private int idCliente;
    private int idVoucher;
    private String estacionamiento;
    private int valorTotal;

    public VoucherVO() {
    }

    public VoucherVO(int idCliente, int idVoucher, String estacionamiento, int valorTotal) {
        this.idCliente = idCliente;
        this.idVoucher = idVoucher;
        this.estacionamiento = estacionamiento;
        this.valorTotal = valorTotal;
    }

    /**
     * @return the idCliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the idVoucher
     */
    public int getIdVoucher() {
        return idVoucher;
    }

    /**
     * @param idVoucher the idVoucher to set
     */
    public void setIdVoucher(int idVoucher) {
        this.idVoucher = idVoucher;
    }

    /**
     * @return the estacionamiento
     */
    public String getEstacionamiento() {
        return estacionamiento;
    }

    /**
     * @param estacionamiento the estacionamiento to set
     */
    public void setEstacionamiento(String estacionamiento) {
        this.estacionamiento = estacionamiento;
    }

    /**
     * @return the valorTotal
     */
    public int getValorTotal() {
        return valorTotal;
    }

    /**
     * @param valorTotal the valorTotal to set
     */
    public void setValorTotal(int valorTotal) {
        this.valorTotal = valorTotal;
    }

}
