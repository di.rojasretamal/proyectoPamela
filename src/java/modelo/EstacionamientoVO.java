/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Nicolas
 */
public class EstacionamientoVO {

    private int idEstacionamiento;
    private String tipoEstacionamiento;
    private int numeroTicket;
    private int valorHora;

    public EstacionamientoVO() {
    }

    public EstacionamientoVO(int idEstacionamiento, String tipoEstacionamiento, int numeroTicket, int valorHora) {
        this.idEstacionamiento = idEstacionamiento;
        this.tipoEstacionamiento = tipoEstacionamiento;
        this.numeroTicket = numeroTicket;
        this.valorHora = valorHora;
    }
    
    
    /**
     * @return the idEstacionamiento
     */
    public int getIdEstacionamiento() {
        return idEstacionamiento;
    }

    /**
     * @param idEstacionamiento the idEstacionamiento to set
     */
    public void setIdEstacionamiento(int idEstacionamiento) {
        this.idEstacionamiento = idEstacionamiento;
    }

    /**
     * @return the tipoEstacionamiento
     */
    public String getTipoEstacionamiento() {
        return tipoEstacionamiento;
    }

    /**
     * @param tipoEstacionamiento the tipoEstacionamiento to set
     */
    public void setTipoEstacionamiento(String tipoEstacionamiento) {
        this.tipoEstacionamiento = tipoEstacionamiento;
    }

    /**
     * @return the numeroTicket
     */
    public int getNumeroTicket() {
        return numeroTicket;
    }

    /**
     * @param numeroTicket the numeroTicket to set
     */
    public void setNumeroTicket(int numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    /**
     * @return the valorHora
     */
    public int getValorHora() {
        return valorHora;
    }

    /**
     * @param valorHora the valorHora to set
     */
    public void setValorHora(int valorHora) {
        this.valorHora = valorHora;
    }
    
    
    
    
}
