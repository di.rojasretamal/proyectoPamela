
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Duoc
 */
public class OpcionEnvioADO {
    
    private Connection cx;
    public OpcionEnvioADO(Connection cx)
    {
        this.cx =cx;
    }

    public int ingresarOpcionEnvio(OpcionEnvioVO opcion)
    {
        String sql = "insert into opcionEnvio values(?,?)";
        
        try {
          
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, opcion.getIdEnvio());
            ps.setString(2, opcion.getTipoEnvio());
            
            
            
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(OpcionEnvioADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public int eliminarOpcion(int opcion)
    {
        String sql = "delete from opcionEnvio where idEnvio=?";
        
        try {
            
            PreparedStatement ps = this.cx.prepareStatement(sql);
            ps.setInt(1, opcion);
            int c = ps.executeUpdate();
            
            return c;
        } catch (SQLException ex) {
            
            Logger.getLogger(OpcionEnvioADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public int actualizarOpcion(OpcionEnvioVO opcion)
    {
       
        String sql= "update opcionEnvio set tipoEnvio=? where idEnvio=?";
        try {
            PreparedStatement ps= this.cx.prepareStatement(sql);
            ps.setInt(1, opcion.getIdEnvio());
            ps.setString(2, opcion.getTipoEnvio());
            
            
            int c=ps.executeUpdate();
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(OpcionEnvioADO.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    
    public ArrayList<OpcionEnvioVO> buscarTodos(){
            ArrayList<OpcionEnvioVO> temp=new ArrayList<OpcionEnvioVO>();
                    String sql="select * from opcionEnvio";
                    Statement stm = null;
                    ResultSet rs =null;
        try {
                    stm= this.cx.createStatement();
                    rs=stm.executeQuery(sql);
                    while(rs.next()){
                        temp.add(new OpcionEnvioVO(rs.getInt(1),rs.getString(2)));
                    }
                    rs.close();
                    stm.close();
                    return temp;
                    
        } catch (SQLException ex) {
            Logger.getLogger(OpcionEnvioADO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
}
}
