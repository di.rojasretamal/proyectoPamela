<%-- 
    Document   : ingresoCliente
    Created on : 02-11-2017, 21:41:32
    Author     : Nicolas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AutoPark</title>
        <style>
            .tituloPrincipal{
                font-size: 25px;
                margin-bottom: 20px;
                text-align: center;
            }

            input{
                margin: 5px;
            }

            .buttonSeleccionar{
                border: none;
                width: 200px;
                height: 60px;
                font-size: 20px;
                margin: 10px;
                background-color: khaki;
            }

            .buttonSeleccionar:hover{
                background-color: yellow;
                border-style: solid;
                border-width: 5px;
            }

            .imagenCarro{
                background-image: url(img/estacionamiento.jpg);
                width: 150px;
                height: 150px;
                background-repeat: no-repeat;
            }

            .menuLateralIzquierdo{
                float: left;
                width: 25%;
            }

            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
        </style>  
    </head>
    <body>
        <div class="tituloPrincipal"> Datos Empresa </div>
        <div class="menuLateralIzquierdo">                
            <div class="imagenCarro"></div>                
            <a href="www.fb.com">Inicio</a><br/>
            <a href="www.fb.com">Ver pagos/Compra</a><br/>
            <a href="www.fb.com">Ayuda</a><br/>
            <p>Opciones de Pago</p>
            <form action="">
                <input type="radio" name="pago" value="Transferencia">Transferencia<br>
                <input type="radio" name="pago" value="PagoLinea">Pago Linea<br>
                <input type="radio" name="pago" value="OrdenCompra">Orden Compra
            </form>
            <p>Opciones de Envio Boleta</p>
            <form action="">
                <input type="radio" name="opcionEnvio" value="correoElectronico">Correo Electronico<br>
                <input type="radio" name="opcionEnvio" value="direccionParticular">Direccion Particular
            </form>
        </div>
        <div style="float:left; width: 49%;">
            <!--<form name="formSand" method="post" action="IngresarCliente.do">-->
            <div><label>Rut        : </label><input type="text" name="idRut"/></div>
            <div><label>Nombre     : </label><input type="text" name="idNombre"/></div>
            <div><label>Telefono   : </label><input type="text" name="idTelefono"/></div>
            <div><label>Email      : </label><input type="text" name="idEmail"/></div>
            <!--<input type="submit" value="Registrar Cliente" class="buttonSeleccionar"><br>-->
            <!--</form>-->
            <p>Seleccione estacionamiento, indique la cantidad de dinero que mostro aplicación movil.</p>
            <select>
                <option value="Estacionamiento">Estacionamiento</option>
                <option value="Estacionamiento">Estacionamiento</option>
                <option value="Estacionamiento">Estacionamiento</option>
                <option value="Estacionamiento">Estacionamiento</option>
            </select>

            <table style="width:100%; margin-top: 20px;">
                <tr>
                    <th>Estacionamiento</th>
                    <th>Monto</th> 
                    <th>Numero Ticket</th>
                    <th></th>
                </tr>
                <tr>
                    <td>Plaza valparaiso</td>
                    <td>2000</td> 
                    <td>234234234</td>
                    <td>Age</td>
                </tr>
                <tr>
                    <td>Otro Lugar</td>
                    <td>2000</td> 
                    <td>234234234</td>
                    <td>Age</td>
                </tr>
            </table>
            
            <input class="buttonSeleccionar" name="botonSeleccionar" type="submit" value="Pagar">
            
        </div>
        <div style="float: left; width: 25%;">
            <a href="www.fb.com">Ver Estacionamientos</a>
        </div>    
    </body>
</html>

